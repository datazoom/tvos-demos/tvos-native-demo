//
//  ViewController.swift
//  NativeDemo
//
//  Created by Vishnu M P on 27/12/18.
//  Copyright © 2018 Adhoc Technologies. All rights reserved.
//

import UIKit
import AVKit
import Foundation
import MediaPlayer
import AVFoundation
import DZNativeCollector

class ViewController: UIViewController {
    
    @IBOutlet weak var textFieldConfigId        : UITextField!
    @IBOutlet weak var labelConnectingURL       : UILabel!
    @IBOutlet weak var envrmntSegment           : UISegmentedControl!
    
    var configId:String         = ""
    let devURL:String           = "https://devplatform.datazoom.io/beacon/v1/"
    let qaURL:String            = "https://stagingplatform.datazoom.io/beacon/v1/"
    let productionURL:String    = "https://platform.datazoom.io/beacon/v1/"
    let preProductionURL:String = "https://platform.datazoom.io/beacon/v1/"

    var connectionURL:String    = ""
    var videoPlayer             = AVPlayer()
    var globalAssetValues       = ""
    
    var buttonPushMe            : UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()
        //self.textFieldConfigId.text = "d487f677-3ffc-4d02-ae7b-8e84a84855a0"
        self.configId = textFieldConfigId.text!
        self.labelConnectingURL.text = devURL
        self.connectionURL = devURL
    }

    //MARK:- Button Play Touched
    @IBAction func buttonPlayTouched(_ sender: UIButton) {
        if validateConfigId(configIdL: self.configId) {
            globalAssetValues = "https://thenewcode.com/assets/videos/blue.mp4" //"https://thenewcode.com/assets/videos/before.mp4" //"https://thenewcode.com/assets/videos/glacier.mp4"
            self.loadVideo(urlString:globalAssetValues )
        }else {
            self.showInvalidConfigIdAlert()
            return
        }
    }
    
    //MARK:- Did Change Environment
    @IBAction func didChangeEnvironment(_ sender: Any) {
        switch envrmntSegment.selectedSegmentIndex {
        case 0:
            self.connectionURL = self.devURL
            print("Dev selected \(self.connectionURL)")
            self.labelConnectingURL.text = self.connectionURL
        case 1:
            self.connectionURL = self.qaURL
            print("QA selected \(self.connectionURL)")
            self.labelConnectingURL.text = self.connectionURL
        case 2:
            self.connectionURL = self.preProductionURL
            print("Production selected \(self.connectionURL)")
            self.labelConnectingURL.text = self.connectionURL
        case 3:
            self.connectionURL = self.productionURL
            print("Production selected \(self.connectionURL)")
            self.labelConnectingURL.text = self.connectionURL
        default:
            break
        }
    }
    
    //MARK:- Validate Config Id
    func validateConfigId(configIdL:String) -> Bool {
        if configIdL.range(of: "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}", options: .regularExpression, range: nil, locale: nil) != nil {
            return true
        }else {
            return false
        }
    }
    
    //MARK:- Loading the Native Video player method.
    func loadVideo(urlString: String) {
        
        let videoURL = URL(string: urlString)
        videoPlayer = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = videoPlayer
        self.present(playerViewController, animated: true) {
             playerViewController.player!.play()
            playerViewController.showsPlaybackControls = true
            UIApplication.shared.beginReceivingRemoteControlEvents()
            self.initialiseWithConfigValue()
            self.buttonPushMe = UIButton(frame: CGRect(x: (self.view.frame.width / 2) - 150, y: 50, width: 300, height: 100))
            self.buttonPushMe.layer.cornerRadius = 10.0
            self.buttonPushMe.setTitle("Push Me", for: .normal)
            self.buttonPushMe.setTitleColor(UIColor.white, for: .normal)
            self.buttonPushMe.backgroundColor = UIColor(red: 34/255, green: 218/255, blue: 34/255, alpha: 1.0)
            self.buttonPushMe.addTarget(self, action: #selector(self.buttonPushTouched), for: .touchUpInside)
            playerViewController.view.window!.addSubview(self.buttonPushMe)
        }
    }
    
    func initialiseWithConfigValue() {
        DZNativeCollector.dzSharedManager.flagForAutomationOnly = false
        DZNativeCollector.dzSharedManager.fluxDataFlag = true
        DZNativeCollector.dzSharedManager.initNativePlayerWith(configID: self.configId, url: self.connectionURL, playerInstance: videoPlayer)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            let DZURL = NSURL(string: self.connectionURL)
            let domain = (DZURL?.host)!
            DZNativeCollector.dzSharedManager.customMetaData(["customPlayerName" : "iOS Native Player","customDomain":domain])
            DZNativeCollector.dzSharedManager.customEvnets("SDKLoaded", metadata: nil)
        }
    }
    
    //MARK:- Show Invalid ConfigId Alert
    func showInvalidConfigIdAlert() {
        let alert = UIAlertController(title: "", message: "Please entwr a valid Configuration Id.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    @objc func buttonPushTouched() {
          DZNativeCollector.dzSharedManager.customEvnets("buttonPush", metadata: nil)
    }
}

extension ViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "\n" {
            textFieldConfigId.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.configId = textFieldConfigId.text!
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldConfigId.resignFirstResponder()
        return true
    }
}
